# Network Time Security (NTS) client in Go

This is a small
[NTS](https://datatracker.ietf.org/doc/draft-ietf-ntp-using-nts-for-ntp/)
client in Go to query for authenticated time. Most of the work is done
by:

- [ntske](https://gitlab.com/hacklunch/ntske) NTS Key Exchange Go library
- [ntp](https://github.com/mchackorg/ntp) NTP/NTS Go library

Usage:

```
ntsclient -configfile /etc/ntsclient.toml -set
```

This will read a configuration file and actually attempt to set system
time. Using `-set` means ntsclient will have to run as root (on many
systems) or be awarded some capability or similar.

On Linux, set the CAP_SYS_TIME capability like so:

    sudo setcap CAP_SYS_TIME+ep ./ntsclient

See the `ntsclient.toml` for configuration suggestions.
