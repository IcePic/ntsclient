module gitlab.com/hacklunch/ntsclient

require (
	github.com/BurntSushi/toml v0.3.1
	gitlab.com/hacklunch/ntp v0.2.1-0.20190819112626-2096ac2a0a1a
	gitlab.com/hacklunch/ntske v0.0.0-20190723102943-5768e20d64c4
)
