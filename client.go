package main

import (
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"syscall"
	"time"

	"github.com/BurntSushi/toml"
	"gitlab.com/hacklunch/ntp"
	"gitlab.com/hacklunch/ntske"
)

type Config struct {
	ServerPort string
	CACertFile string
	Interval   time.Duration
}

func printmeta(meta ntske.Data) {
	fmt.Printf("NTSKE exchange yielded:\n"+
		"  c2s: %x\n"+
		"  s2c: %x\n"+
		"  server: %v\n"+
		"  port: %v\n"+
		"  algo: %v\n",
		string(meta.C2sKey),
		string(meta.S2cKey),
		meta.Server,
		meta.Port,
		meta.Algo,
	)

	fmt.Printf("  %v cookies:\n", len(meta.Cookie))
	for i, cookie := range meta.Cookie {
		fmt.Printf("  #%v: %x\n", i+1, cookie)
	}
}

func loadConfig(configfile string) (*Config, error) {
	contents, err := ioutil.ReadFile(configfile)
	if err != nil {
		return nil, err
	}

	conf := new(Config)

	if _, err := toml.Decode(string(contents), &conf); err != nil {
		return nil, err
	}

	return conf, nil
}

func tlsSetup(cacert string, dontValidate bool) (*tls.Config, error) {
	// Enable experimental TLS 1.3
	os.Setenv("GODEBUG", os.Getenv("GODEBUG")+",tls13=1")

	certPool := x509.NewCertPool()
	if cacert == "" {
		certPool, _ = x509.SystemCertPool()
	} else {
		certs, err := ioutil.ReadFile(cacert)
		if err != nil {
			return nil, fmt.Errorf("failed to append %s to certPool: %v", cacert, err)
		}

		if ok := certPool.AppendCertsFromPEM(certs); !ok {
			return nil, fmt.Errorf("couldn't appended cert %v: %v", cacert, err)
		}
	}

	c := tls.Config{RootCAs: certPool}
	if dontValidate {
		c.InsecureSkipVerify = true
	}

	return &c, nil
}

func keyExchange(server string, c tls.Config, debug bool) (*ntske.KeyExchange, error) {
	ke, err := ntske.Connect(server, c, debug)
	if err != nil {
		return nil, fmt.Errorf("connection failure to %v: %v", server, err)
	}

	err = ke.Exchange()
	if err != nil {
		return nil, fmt.Errorf("NTS-KE exchange error: %v", err)
	}

	if len(ke.Meta.Cookie) == 0 {
		return nil, fmt.Errorf("received no cookies")
	}

	if ke.Meta.Algo != ntske.AES_SIV_CMAC_256 {
		return nil, fmt.Errorf("unknown algorithm in NTS-KE")
	}

	err = ke.ExportKeys()
	if err != nil {
		return nil, fmt.Errorf("export key failure: %v", err)
	}

	return ke, nil
}

// Needs root or capability to set time. Should probably run in its own process.
func setTime(t time.Time) error {
	tv := syscall.NsecToTimeval(t.UnixNano())
	err := syscall.Settimeofday(&tv)
	if err != nil {
		return err
	}

	return nil
}

func main() {
	var dontValidate bool
	var debug bool
	var configfile string
	var set bool

	flag.StringVar(&configfile, "config", "./ntsclient.toml", "Path to configuration file")
	flag.BoolVar(&dontValidate, "dontvalidate", false, "don't validate certs")
	flag.BoolVar(&debug, "debug", false, "Turn on debug output")
	flag.BoolVar(&set, "set", false, "Actually set system time. Requires root or capability.")

	flag.Parse()

	conf, err := loadConfig(configfile)
	if err != nil {
		log.Fatalf("couldn't load configuration file: %v", err)
	}

	tlsconfig, err := tlsSetup(conf.CACertFile, dontValidate)
	if err != nil {
		log.Fatalf("Couldn't set up TLS: %v", err)
	}

	for {
		ke, err := keyExchange(conf.ServerPort, *tlsconfig, debug)
		if err != nil {
			log.Printf("key exchange failed: %v", err)
			time.Sleep(10 * time.Second)
			continue
		}

		if debug {
			printmeta(ke.Meta)
		}

		var opt ntp.QueryOptions
		opt.Port = int(ke.Meta.Port)
		opt.NTS = true
		opt.C2s = ke.Meta.C2sKey
		opt.S2c = ke.Meta.S2cKey
		opt.Cookie = ke.Meta.Cookie[0]
		if debug {
			opt.Debug = true
		}

		nrcookies := len(ke.Meta.Cookie)

		for n := 0; n < nrcookies; n++ {
			resp, err := ntp.QueryWithOptions(ke.Meta.Server, opt)
			if err != nil {
				log.Printf("NTP query failed: %v", err)
				goto sleep
			}

			if debug {
				log.Printf("response: %#v\n", resp)
			}

			err = resp.Validate()
			if err != nil {
				log.Printf("NTP response validation error: %v", err)
				goto sleep
			}

			if set {
				err := setTime(time.Now().Add(resp.ClockOffset))
				if err != nil {
					log.Printf("Couldn't set system time: %v", err)
				}
			} else {
				log.Printf("Network time on %v:%v %v. Local clock off by %v.\n", ke.Meta.Server, ke.Meta.Port, resp.Time, resp.ClockOffset)
			}

		sleep:
			time.Sleep(conf.Interval * time.Minute)
		}
	}
}
